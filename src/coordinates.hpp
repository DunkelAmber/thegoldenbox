#ifndef COORDNATES_HPP
#define COORDNATES_HPP

#include <iostream>
#include <string>

/*!
 *  \class     Coordinates
 *  \brief     Coordinates example
 *  \details   This class is used to demonstrate a possible solution for the Golden Box problem.
 *  \author    Francesco Masciari
 *  \version   1.0.0
 *  \date      2018-10-20
 *  \copyright MIT Licence
 */

class Coordinates
{
public:
    Coordinates(float _x=0, float _y=0, float _z=0, float _pitch=0, float _roll=0, float _yaw=0);

    float getX();
    float getY();
    float getZ();
    float getPitch();
    float getRoll();
    float getYaw();

    void setX(float val);
    void setY(float val);
    void setZ(float val);
    void setPitch(float val);
    void setRoll(float val);
    void setYaw(float val);

    Coordinates calculateNewCoordinate (float distance);
    static double getDistance(Coordinates a, Coordinates b);

    void copy(Coordinates source);
    void print();
    void print(std::string name);

private:
    float x, y, z, pitch, roll, yaw;
};

#endif // COORDNATES_HPP
