/*! \mainpage The Golden Box Index Page
 *
 * \section intro_sec Introduction
 *
 * ## Question
 * \image html question.png width=1000
 *
 * \section Math
 *
 * \subsection Coordinates
 * \image html trigonometry_black.png width=340
 *
 * - **distance** is the module of the distance from the drone to the goldenBox as returned by `getDistance()`
 * - **distanceXY** is the projection of **distance** on XY plane
 * - **roll**, **pitch** and **yaw** rapresent the drone's pose
 *
 * \subsection Graph
 * \image html graph.png width=500
 *
 * - Green is _Pete's house_
 * - Red is _the drone_
 * - Yellow is _the golden box_
 *
 */

#include "coordinates.hpp"
#include <iostream>

float getDistance()
{
    float dist = 1;
    std::cout << "\n# Insert distance [meters] \033[31m»\033[0m " << '\t';
    std::cin >> dist;
    return dist;
}

int main(int argc, char const *argv[]) {

    Coordinates *pete = new Coordinates ();
    Coordinates *drone = new Coordinates (1, 1, 1, -45, 0, 0);
    Coordinates *goldenBox = new Coordinates ();

    pete->print("Pete's House");
    drone->print("Drone");
    goldenBox->print("Golden Box");

    goldenBox->copy( drone->calculateNewCoordinate(getDistance()) );

    pete->print("\nPete's House");
    drone->print("Drone");
    goldenBox->print("Golden Box");

    std::cout << "\nDistance calculated\t" << Coordinates::getDistance(*drone, *goldenBox) << std::endl;

    return 0;
}
