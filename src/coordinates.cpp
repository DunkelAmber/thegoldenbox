#include "coordinates.hpp"
#include <math.h>

/**
 *  \param _x X position coordinate. In meters.
 *  \param _y Y positino coordinate. In meters.
 *  \param _x Z position coordinate. In meters.
 *  \param _pitch Pitch orientation. In degrees.
 *  \param _roll Roll orientation. In degrees.
 *  \param _yaw Yaw orientation. In degrees.
 *
 */

#define CONV_COEFF_RAD_DEG 0.01745329252    // Conversion coefficiant to convert degree to radiant

Coordinates::Coordinates(float _x, float _y, float _z, float _pitch, float _roll, float _yaw)
{
    x = _x;
    y = _y;
    z = _z;
    pitch = _pitch*CONV_COEFF_RAD_DEG;
    roll = _roll*CONV_COEFF_RAD_DEG;
    yaw = _yaw*CONV_COEFF_RAD_DEG;
}

/**
 *  \brief Calculate coordinates at a given distance.
 *  It gives the coordiantes depending on the starting coordinates
 *  and a given distance from them.
 *
 *  \param distance The distance from the starting coordinates
 *  \return target A coordinates instance
 */
Coordinates Coordinates::calculateNewCoordinate (float distance)
{
    Coordinates target;

    float distance_XY = distance * cos(this->getPitch());

    target.setX( this->getX() +  distance_XY*cos(this->getYaw()) );
    target.setY( this->getY() +  distance_XY*sin(this->getYaw()) );
    target.setZ( this->getZ() +  distance*sin(this->getPitch()) );

    return target;
}

/**
 *  \brief It gives the distance between two coordinates
 *  \param a First coordiantes
 *  \param b Second coordinates
 *  \return ans The distance between the two coordinates
 */
double Coordinates::getDistance(Coordinates a, Coordinates b)
{
    double x1 = a.getX();
    double y1 = a.getY();
    double z1 = a.getZ();

    double x2 = b.getX();
    double y2 = b.getY();
    double z2 = b.getZ();

    double sum2 = pow(x2-x1, 2) + pow(y2-y1, 2) + pow(z2-z1, 2);
    float ans = sqrt(sum2);

    return ans;
}



/**
 *  \brief Copy to coordinates form another coordinates instance.
 *  \param source Coordinates object from which copy
 */
void Coordinates::copy(Coordinates source)
{
    this->setX(source.getX());
    this->setY(source.getY());
    this->setZ(source.getZ());
    this->setPitch(source.getPitch());
    this->setRoll(source.getRoll());
    this->setYaw(source.getYaw());
}

/*
 *  Printers
 */
void Coordinates::print()
{
    std::cout << "(" << x << " " << y << " " << z << " " << pitch << " " << roll << " " << yaw << ")" << std::endl;
}

void Coordinates::print(std::string name)
{
    std::cout << name << " (" << x << " " << y << " " << z << " " << pitch << " " << roll << " " << yaw << ")" << std::endl;
}

/*
 *  Getters
 */
float Coordinates::getX ()
{
    return x;
}

float Coordinates::getY ()
{
    return y;
}

float Coordinates::getZ ()
{
    return z;
}

float Coordinates::getPitch ()
{
    return pitch;
}

float Coordinates::getRoll ()
{
    return roll;
}

float Coordinates::getYaw ()
{
    return yaw;
}

/*
*  Setters
*/
void Coordinates::setX (float val)
{
    x = val;
}

void Coordinates::setY (float val)
{
    y = val;
}

void Coordinates::setZ (float val)
{
    z = val;
}

void Coordinates::setPitch (float val)
{
    pitch = val;
}

void Coordinates::setRoll (float val)
{
    roll = val;
}

void Coordinates::setYaw (float val)
{
    yaw = val;
}
