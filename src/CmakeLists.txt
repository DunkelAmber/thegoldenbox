cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
project(goldenbox)

add_executable(goldenBox main.cpp coordinates.hpp coordinates.cpp)
