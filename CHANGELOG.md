# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [v1.2.1] - 2018-10-30
### Fixed
- Global Makefile wrong compilation path.

## [v1.2.0] - 2018-10-30
### Added
- Global Makefile: create URI shortcut to doxygen mainpage.

### Changed
- Folder structure

## [v1.1.0] - 2018-10-30
### Added
- Colors to prompt.
- Doxygen documentation.
- Global Makefile for faster compilation and run.
- Documentation elements as images.

### Changed
- Prompt style.
- Improve readability: change name to Coordinates class variables.

## [v1.0.2] - 2018-10-21
### Changed
- 3D Graph for documentation for Apple Graph: minor change.

## [v1.0.1] - 2018-10-20
### Added
- Readme file.
- 3D Graph for documentation for Apple Graph.
- 3D Graph for documentation as MOV file.

## [v1.0.0] - 2018-10-20
### Added
- Main code structure.
