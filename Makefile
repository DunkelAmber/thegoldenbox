all: compile run

compile:
	@cd src &&\
	mkdir -p build &&\
	cd build &&\
	echo "\n\033[1;33m█ Generating makefiles...\033[0m\n" &&\
	cmake .. &&\
	echo "\n\033[1;93m█ Compiling...\033[0m\n" &&\
	make

run:
	echo "\n\033[1;32m█ Running...\033[0m\n\n" &&\
	./src/build/goldenBox

dox:
	@echo "\n\033[1;32m█ Generating documentation with Doxygen...\033[0m\n"
	@doxygen doxygen/doxConf
	@rm -f dox.url
	@echo "[InternetShortcut]\nURL=file://"$(shell pwd)"/doc/html/index.html" > dox.url
	@echo "\n\033[1;32m█ Generation of documentation with Doxygen: SUCCEDED\n  Check dox.url in the main project folder\033[0m\n"
