# theGoldenBox
A possible solution to the Golden Box question

## The Problem
![theproblem](images/question.png)

## Dependencies
### For Ubuntu

```bash
sudo apt install cmake doxygen
```

If cmake does not work, check you PATH variable: it should contain `/usr/local/bin`.

This can be solved with the following command:

```bash
echo 'export PATH=$PATH:/usr/local/bin'  >> ~/.bashrc
```

### For macOS

```bash
brew install cmake doxygen
```
If cmake does not work, check you PATH variable: it should contain `/usr/local/bin`.

This can be solved with the following command:

```bash
echo 'export PATH=$PATH:/usr/local/bin'  >> ~/.bash_profile
```

## Compile & Run
Inside the root project folder:

### - with Global Makefile
```bash
make
```
It actually execute the following commands (see Compile & Run with CMake).

### - with CMake

```bash
cd src
mkdir -p build
cd build
cmake ..
make
./goldenBox
```
### - with g++

```bash
cd src
g++  main.cpp coordinates.hpp coordinates.cpp
./a.out
```

## Math
### Coordinates
![equation_trig](images/trigonometry_black_w250.png)

- **distance** is the module of the distance from the drone to the goldenBox as returned by `getDistance()`
- **distanceXY** is the projection of **distance** on XY plane
- **roll**, **pitch** and **yaw** represent the drone's pose

### Graph
![graph](images/graph_w350.png)

- Green is _Pete's house_
- Red is _the drone_
- Yellow is _the golden box_

## Documentation

Create documentation

### - with Global Makefile
```bash
make dox
```
and open `dox.url` or `doc/html/index.html`

### - with CLI

```bash
doxygen doxygen/doxConf
```
and open `doc/html/index.html`

